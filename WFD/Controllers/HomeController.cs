﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFD.Models;
using clsConstant = WFD.Models.clsConstant;
using Pager = WFD.Models.Pager;

namespace WFD.Controllers
{
    public class HomeController : Controller
        
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
        DataClasses1DataContext db = new DataClasses1DataContext();
        clsCommon objCommon = new clsCommon();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult getCount(clsHome cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls = cls.CountTicket(cls);
                    return Json(cls, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                //  objCommon.ErrorHistoryLog("", "Home", "Count", ex.GetType().ToString(), ex.Message, ex.StackTrace, 0);
                return null;
            }

        }

    }
}