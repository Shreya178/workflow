﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFD.Models;
using clsConstant = WFD.Models.clsConstant;
using Pager = WFD.Models.Pager;
namespace WFD.Controllers
{
    public class DesignerController : Controller
    {
        // GET: Designer
        DataClasses1DataContext db = new DataClasses1DataContext();
        clsCommon objCommon = new clsCommon();  
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Design(clsDesigner cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);

            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    con.Open();
                    List<clsDesigner> lstDesignlist = new List<clsDesigner>();
                    SqlCommand cmd = new SqlCommand("Sp_Get_Design", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@intId", cls.intId);
                    cmd.Parameters.AddWithValue("@intPageSize", cls.intPageSize);
                    cmd.Parameters.AddWithValue("@intPageIndex", cls.intPageIndex);
                    cmd.Parameters.AddWithValue("@strSearchText", cls.strSearchText);
                    cmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    da.Fill(dt);
                    con.Close();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            clsDesigner obj = new clsDesigner();
                            obj.intId = Convert.ToInt32(dt.Rows[i]["intId"] == null || dt.Rows[i]["intId"].ToString().Trim() == "" ? null : dt.Rows[i]["intId"].ToString());
                            obj.intQty = Convert.ToInt32(dt.Rows[i]["intQty"] == null || dt.Rows[i]["intQty"].ToString().Trim() == "" ? null : dt.Rows[i]["intQty"].ToString());
                            obj.strMaterial = dt.Rows[i]["strMaterial"] == null || dt.Rows[i]["strMaterial"].ToString().Trim() == "" ? null : dt.Rows[i]["strMaterial"].ToString();
                            obj.strPId = Convert.ToInt32(dt.Rows[i]["strPId"] == null || dt.Rows[i]["strPId"].ToString().Trim() == "" ? null : dt.Rows[i]["strPId"].ToString());
                            obj.strDesign = dt.Rows[i]["strDesign"] == null || dt.Rows[i]["strDesign"].ToString().Trim() == "" ? null : dt.Rows[i]["strDesign"].ToString();
                            obj.Design = dt.Rows[i]["DesignFile"] == null || dt.Rows[i]["DesignFile"].ToString().Trim() == "" ? null : dt.Rows[i]["DesignFile"].ToString();
                            obj.intSize = Convert.ToInt32(dt.Rows[i]["intSize"] == null || dt.Rows[i]["intSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intSize"].ToString());
                            obj.intPrice = Convert.ToInt32(dt.Rows[i]["intPrice"] == null || dt.Rows[i]["intPrice"].ToString().Trim() == "" ? null : dt.Rows[i]["intPrice"].ToString());
                            //obj.strFileName = dt.Rows[i]["strDesign"] == null || dt.Rows[i]["strDesign"].ToString().Trim() == "" ? null : dt.Rows[i]["strDesign"].ToString();
                            obj.intPageCount = Convert.ToInt32(dt.Rows[i]["intPageCount"] == null || dt.Rows[i]["intPageCount"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageCount"].ToString());
                            obj.intCategory = Convert.ToInt32(dt.Rows[i]["intGender"] == null || dt.Rows[i]["intGender"].ToString().Trim() == "" ? null : dt.Rows[i]["intGender"].ToString());
                            obj.intPageSize = Convert.ToInt32(dt.Rows[i]["intPageSize"] == null || dt.Rows[i]["intPageSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageSize"].ToString());
                            obj.intPageIndex = Convert.ToInt32(dt.Rows[i]["intPageIndex"] == null || dt.Rows[i]["intPageIndex"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageIndex"].ToString());
                            obj.intTotalRecord = Convert.ToInt32(dt.Rows[i]["intTotalRecord"] == null || dt.Rows[i]["intTotalRecord"].ToString().Trim() == "" ? null : dt.Rows[i]["intTotalRecord"].ToString());
                            lstDesignlist.Add(obj);
                        }
                    }
                    cls.LSTdesignList = lstDesignlist;

                    return View("Design", cls);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                //objclsCommonFunction.SendErrorMail(ex, "");
                return RedirectToAction("Login", "Login");
            }
        }


        public ActionResult ViewDesign()
        {
            return View();
        }
        public ActionResult InsertDesign(clsDesigner cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.InsertData;
                    string strProfile = null;
                    if (cls.strProfile != null)
                    {
                        string strFile = DateTime.Now.Ticks.ToString();
                        string file = cls.strProfile.FileName;
                        string ext = System.IO.Path.GetExtension(cls.strProfile.FileName);
                        strFile = strFile + ext;
                        cls.strProfile.SaveAs(Server.MapPath("~/Content/image/" + strFile));
                        strProfile = strFile;
                        cls.strFileName = strFile;
                        //clr.Profile.SaveAs(HttpPostedFile.MapPath("~/Content/File/" + strFile));
                    }
                    string number = "0123456789";
                    string generated = "!";
                    Random random = new Random();
                    for (int i = 1; i <= 4; i++)
                        generated = generated.Insert(
                            random.Next(generated.Length),
                            number[random.Next(number.Length - 1)].ToString()
                        );
                    cls.strPId = Convert.ToInt32(generated.Replace("!", string.Empty));
                    cls = cls.InsertDesign(cls);
                    return Json(cls.strResponse, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }

            catch (Exception ex)
            {
                return RedirectToAction("Login", "Login");
            }
        }
        public ActionResult ViewDesigner(clsDesigner cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {

                    int intTotalEntries = 0;
                    int intshowingEntries = 0;
                    int startentries = 0;
                    con.Open();
                    List<clsDesigner> lstDesignlist = new List<clsDesigner>();
                    SqlCommand cmd = new SqlCommand("Sp_Get_Design", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@intId", cls.intId);
                    cmd.Parameters.AddWithValue("@intPageSize", cls.intPageSize);
                    cmd.Parameters.AddWithValue("@intPageIndex", cls.intPageIndex);
                    cmd.Parameters.AddWithValue("@strSearchText", cls.strSearchText);
                    cmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    da.Fill(dt);
                    con.Close();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            clsDesigner obj = new clsDesigner();
                            obj.intId = Convert.ToInt32(dt.Rows[i]["intId"] == null || dt.Rows[i]["intId"].ToString().Trim() == "" ? null : dt.Rows[i]["intId"].ToString());
                            obj.intQty = Convert.ToInt32(dt.Rows[i]["intQty"] == null || dt.Rows[i]["intQty"].ToString().Trim() == "" ? null : dt.Rows[i]["intQty"].ToString());
                            obj.strMaterial = dt.Rows[i]["strMaterial"] == null || dt.Rows[i]["strMaterial"].ToString().Trim() == "" ? null : dt.Rows[i]["strMaterial"].ToString();
                            obj.strPId = Convert.ToInt32(dt.Rows[i]["strPId"] == null || dt.Rows[i]["strPId"].ToString().Trim() == "" ? null : dt.Rows[i]["strPId"].ToString());
                            obj.strDesign = dt.Rows[i]["strDesign"] == null || dt.Rows[i]["strDesign"].ToString().Trim() == "" ? null : dt.Rows[i]["strDesign"].ToString();
                            obj.intSize = Convert.ToInt32(dt.Rows[i]["intSize"] == null || dt.Rows[i]["intSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intSize"].ToString());
                            obj.intPrice = Convert.ToInt32(dt.Rows[i]["intPrice"] == null || dt.Rows[i]["intPrice"].ToString().Trim() == "" ? null : dt.Rows[i]["intPrice"].ToString());
                            //obj.strFileName = dt.Rows[i]["strDesign"] == null || dt.Rows[i]["strDesign"].ToString().Trim() == "" ? null : dt.Rows[i]["strDesign"].ToString();
                            obj.intPageCount = Convert.ToInt32(dt.Rows[i]["intPageCount"] == null || dt.Rows[i]["intPageCount"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageCount"].ToString());
                            obj.intCategory = Convert.ToInt32(dt.Rows[i]["intGender"] == null || dt.Rows[i]["intGender"].ToString().Trim() == "" ? null : dt.Rows[i]["intGender"].ToString());
                            obj.intPageSize = Convert.ToInt32(dt.Rows[i]["intPageSize"] == null || dt.Rows[i]["intPageSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageSize"].ToString());
                            obj.intPageIndex = Convert.ToInt32(dt.Rows[i]["intPageIndex"] == null || dt.Rows[i]["intPageIndex"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageIndex"].ToString());
                            obj.intTotalRecord = Convert.ToInt32(dt.Rows[i]["intTotalRecord"] == null || dt.Rows[i]["intTotalRecord"].ToString().Trim() == "" ? null : dt.Rows[i]["intTotalRecord"].ToString());
                            lstDesignlist.Add(obj);

                        }
                    }
                    cls.LSTdesignList = lstDesignlist;
                    if (cls.LSTdesignList.Count > 0)
                    {
                        var pager = new Pager((int)cls.LSTdesignList[0].intTotalRecord, cls.intPageIndex, (int)cls.intPageSize);
                        cls.Pager = pager;
                    }
                    cls.intTotalEntries = intTotalEntries;
                    cls.intShowingEntries = intshowingEntries;
                    cls.intfromEntries = startentries;

                    return PartialView("_DesignerList", cls);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                //objclsCommonFunction.SendErrorMail(ex, "");
                return RedirectToAction("Login", "Login");
            }
        }
        public ActionResult deleteDesign(clsDesigner cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.DeleteData;
                    cls = cls.InsertDesign(cls);
                    return Json(cls, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Login");
            }
        }

        public ActionResult GetSingleDesign(clsDesigner cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    tblDesignMst tbl = new tblDesignMst();
                    var getdata = (from r in db.tblDesignMsts where r.intId == cls.intId select r).FirstOrDefault();
                    return Json(data: getdata);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public ActionResult AddDesign()
        {
            return View();
        }
       
    }
    }
