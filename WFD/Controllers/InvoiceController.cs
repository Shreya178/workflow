﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFD.Models;
using clsConstant = WFD.Models.clsConstant;
using Pager = WFD.Models.Pager;

namespace WFD.Controllers
{
    public class InvoiceController : Controller
    {
        // GET: Invoice
        clsCommon objCommon = new clsCommon();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GenerateInvoice()
        {
            return View();
        }
        public ActionResult GetData(clsInvoice cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls = cls.GetInvoiceData(cls);
                    return Json(cls, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
               // objCommon.ErrorHistoryLog("", "Home", "Count", ex.GetType().ToString(), ex.Message, ex.StackTrace, 0);
                return null;
            }

        }

        public ActionResult InsertInvoice(clsInvoice cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.InsertData;
                    string number = "0123456789";
                    string generated = "!";
                    Random random = new Random();
                    for (int i = 1; i <= 3; i++)
                        generated = generated.Insert(
                            random.Next(generated.Length),
                            number[random.Next(number.Length - 1)].ToString()
                        );
                    cls.intInvoiceNumber = Convert.ToInt32(generated.Replace("!", string.Empty));
                    cls = cls.InsertDetails(cls);
                    return Json(new { data = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }

            catch (Exception ex)
            {
                return RedirectToAction("Login", "Login");
            }
        }
    }
}