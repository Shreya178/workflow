﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFD.Models;
using clsConstant = WFD.Models.clsConstant;
using Pager = WFD.Models.Pager;


namespace WFD.Controllers
{
    public class PurchaseOrderController : Controller
    {
        // GET: PurchaseOrder
        DataClasses1DataContext db = new DataClasses1DataContext();
        clsCommon objCommon = new clsCommon();
        public ActionResult Index()
        {
            return View();
        }
      
        public ActionResult PurchaseOrderDetails()
        {
            return View();
        }
         public ActionResult PurchaseDetails(clsDesigner cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {

                    int intTotalEntries = 0;
                    int intshowingEntries = 0;
                    int startentries = 0;
                    con.Open();
                    
                    List<clsDesigner> lstPurchaselist = new List<clsDesigner>();
                    SqlCommand cmd = new SqlCommand("Sp_Get_PurchaseOrderDetail", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@intId", cls.intId);
                    cmd.Parameters.AddWithValue("@intPageSize", cls.intPageSize);
                    cmd.Parameters.AddWithValue("@intPageIndex", cls.intPageIndex);
                    cmd.Parameters.AddWithValue("@strSearchText", cls.strSearchText);
                    cmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    da.Fill(dt);
                    con.Close();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            clsDesigner obj = new clsDesigner();

                            obj.strCreatedDate = dt.Rows[i]["strCreatedDate"] == null || dt.Rows[i]["strCreatedDate"].ToString().Trim() == "" ? null : Convert.ToDateTime(dt.Rows[i]["strCreatedDate"]).ToString("dd/MM/yyyy");
                            obj.strDueDate = dt.Rows[i]["duedate"] == null || dt.Rows[i]["duedate"].ToString().Trim() == "" ? null : Convert.ToDateTime(dt.Rows[i]["duedate"]).ToString("dd/MM/yyyy");
                            obj.intId = Convert.ToInt32(dt.Rows[i]["intId"] == null || dt.Rows[i]["intId"].ToString().Trim() == "" ? null : dt.Rows[i]["intId"].ToString());
                            obj.intQty = Convert.ToInt32(dt.Rows[i]["intQty"] == null || dt.Rows[i]["intQty"].ToString().Trim() == "" ? null : dt.Rows[i]["intQty"].ToString());
                            obj.strMaterial = dt.Rows[i]["strMaterial"] == null || dt.Rows[i]["strMaterial"].ToString().Trim() == "" ? null : dt.Rows[i]["strMaterial"].ToString();
                            obj.strPId = Convert.ToInt32(dt.Rows[i]["ProductId"] == null || dt.Rows[i]["ProductId"].ToString().Trim() == "" ? null : dt.Rows[i]["ProductId"].ToString());
                            obj.strDesign = dt.Rows[i]["strDesign"] == null || dt.Rows[i]["strDesign"].ToString().Trim() == "" ? null : dt.Rows[i]["strDesign"].ToString();
                            obj.intCategory = Convert.ToInt32(dt.Rows[i]["intGender"] == null || dt.Rows[i]["intGender"].ToString().Trim() == "" ? null : dt.Rows[i]["intGender"].ToString());
                            obj.intSize = Convert.ToInt32(dt.Rows[i]["intSize"] == null || dt.Rows[i]["intSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intSize"].ToString());
                            obj.intQty = Convert.ToInt32(dt.Rows[i]["intQty"] == null || dt.Rows[i]["intQty"].ToString().Trim() == "" ? null : dt.Rows[i]["intQty"].ToString());
                            obj.intPrice = Convert.ToInt32(dt.Rows[i]["intPrice"] == null || dt.Rows[i]["intPrice"].ToString().Trim() == "" ? null : dt.Rows[i]["intPrice"].ToString());
                            obj.intPageCount = Convert.ToInt32(dt.Rows[i]["intPageCount"] == null || dt.Rows[i]["intPageCount"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageCount"].ToString());
                            obj.intPageSize = Convert.ToInt32(dt.Rows[i]["intPageSize"] == null || dt.Rows[i]["intPageSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageSize"].ToString());
                            obj.intPageIndex = Convert.ToInt32(dt.Rows[i]["intPageIndex"] == null || dt.Rows[i]["intPageIndex"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageIndex"].ToString());
                            //obj.Status = Convert.ToInt32(dt.Rows[i]["intStatus"] == null || dt.Rows[i]["intStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["intStatus"].ToString());
                            obj.intTotalRecord = Convert.ToInt32(dt.Rows[i]["intTotalRecord"] == null || dt.Rows[i]["intTotalRecord"].ToString().Trim() == "" ? null : dt.Rows[i]["intTotalRecord"].ToString());
                            obj.intStatus = Convert.ToInt32(dt.Rows[i]["intStatus"] == null || dt.Rows[i]["intStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["intStatus"].ToString());

                            lstPurchaselist.Add(obj);

                        }
                    }
                    cls.LStPurchaseList = lstPurchaselist;
                    if (cls.LStPurchaseList.Count > 0)
                    {
                        var pager = new Pager((int)cls.LStPurchaseList[0].intTotalRecord, cls.intPageIndex, (int)cls.intPageSize);
                        cls.Pager = pager;
                    }
                    cls.intTotalEntries = intTotalEntries;
                    cls.intShowingEntries = intshowingEntries;
                    cls.intfromEntries = startentries;

                    return PartialView("_PurchaseOrderList", cls);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                
                return RedirectToAction("Login", "Login");
            }
        }

        public ActionResult StatusChange(clsDesigner cls)

        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    tblPurcahseMst tbl = new tblPurcahseMst();
                    var updatestatus = (from r in db.tblPurcahseMsts where r.intId == cls.intId select r).FirstOrDefault();
                    updatestatus.intStatus = (updatestatus.intStatus == 0 ? 1 : 1);

                    db.SubmitChanges();
                    return Json(new { data = "status update" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                
                return null;
            }

        }

    }
}