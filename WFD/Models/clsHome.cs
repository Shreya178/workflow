﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WFD.Models
{
    public class clsHome
    {
        public int? NoofDesign { get; set; }
        public int? NoofPurchase { get; set; }
        public clsHome CountTicket(clsHome cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Sp_Count", con);

                cmd.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                System.Data.DataTable dt = new System.Data.DataTable();
                da.Fill(dt);
                con.Close();
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (var i = 0; i < dt.Rows.Count; i++)
                    {
                        clsHome obj = new clsHome();
                        obj.NoofDesign = Convert.ToInt32(dt.Rows[i]["NoofDesign"] == null || dt.Rows[i]["NoofDesign"].ToString().Trim() == "" ? null : dt.Rows[i]["NoofDesign"].ToString());
                        obj.NoofPurchase = Convert.ToInt32(dt.Rows[i]["NoofPurchase"] == null || dt.Rows[i]["NoofPurchase"].ToString().Trim() == "" ? null : dt.Rows[i]["NoofPurchase"].ToString());
                        return (obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return cls;
        }
    }
}