﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WFD.Models
{
    public class clsInvoice
    {
        clsCommon objCommon = new clsCommon();
        public int? intId { get; set; }
        public int? Type { get; set; }
        public int? intCategory { get; set; }
        public DateTime dtDueDate { get; set; }
        public int? intQty { get; set; }
        public int intPrice { get; set; }
        public int? intInvoiceNumber { get; set; }

        public string deliveraddress { get; set; }
        public string strDesign { get; set; }
        public string Design { get; set; }
        public int strPId { get; set; }
        public string strResponse { get; set; }
        public List<clsInvoice> LSTinvoice { get; set; }
        public clsInvoice GetInvoiceData(clsInvoice cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
            try
            {
                con.Open();
                List<clsInvoice> lstinvoice = new List<clsInvoice>();
                SqlCommand cmd = new SqlCommand("Sp_Get_PurchaseOrderDetail", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@intId", cls.intId);
                cmd.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                System.Data.DataTable dt = new System.Data.DataTable();
                da.Fill(dt);
                con.Close();
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (var i = 0; i < dt.Rows.Count; i++)
                    {
                        clsInvoice obj = new clsInvoice();
                        obj.strDesign = dt.Rows[i]["strDesign"] == null || dt.Rows[i]["strDesign"].ToString().Trim() == "" ? null : dt.Rows[i]["strDesign"].ToString();
                        obj.intPrice = Convert.ToInt32(dt.Rows[i]["intPrice"] == null || dt.Rows[i]["intPrice"].ToString().Trim() == "" ? null : dt.Rows[i]["intPrice"].ToString());
                        obj.strPId = Convert.ToInt32(dt.Rows[i]["ProductId"] == null || dt.Rows[i]["ProductId"].ToString().Trim() == "" ? null : dt.Rows[i]["ProductId"].ToString());
                        lstinvoice.Add(obj);
                        cls.LSTinvoice = lstinvoice;
                        return (obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return cls;
        }

        public clsInvoice InsertDetails(clsInvoice cls)
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
                DataClasses1DataContext db = new DataClasses1DataContext();
                conn.Open();
                SqlCommand cmd = new SqlCommand("Sp_InvoiceDetails_Insert", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intId", SqlDbType.Int).Value = cls.intId;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = cls.Type;
                cmd.Parameters.Add("@strDesign", SqlDbType.VarChar).Value = cls.strDesign;
                cmd.Parameters.Add("@strPId", SqlDbType.Int).Value = cls.strPId;
                cmd.Parameters.Add("@intPrice", SqlDbType.Int).Value = cls.intPrice;
                cmd.Parameters.Add("@intInvoiceNumber", SqlDbType.Int).Value = cls.intInvoiceNumber;
                cmd.Parameters.Add("@dtDueDate", SqlDbType.DateTime).Value = cls.dtDueDate;
                cmd.Parameters.Add("@intLoginId", SqlDbType.Int).Value = objCommon.getUserIdFromSession();
                //cmd.Parameters.Add("@strColour", SqlDbType.VarChar).Value = cls.strColour;
                cmd.Parameters.Add("@EntityId", SqlDbType.Int).Value = SqlDbType.Int;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 0;
                da.ReturnProviderSpecificTypes = true;
                DataTable dt = new DataTable();
                da.Fill(dt);

                conn.Close();

                if (dt.Rows.Count > 0)
                {
                    string intRefId = dt.Rows[0][0].ToString();
                    if (intRefId == clsConstant.RECORD_EXISTS)
                    {
                        cls.strResponse = clsConstant.MESSAGE_EXISTS;
                    }
                    else
                    {
                        if (cls.intId == 0)
                        {
                            cls.strResponse = clsConstant.MESSAGE_SUCCESS;
                            cls.intId = Convert.ToInt32(intRefId);
                        }
                        else
                        {
                            cls.strResponse = clsConstant.MESSAGE_UPDATE;
                        }
                    }
                }


            }

            catch (Exception ex)
            {
                throw;
            }

            return cls;
        }

    }
}