﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WFD.Models
{
    public class clsCommon
    {
        DataClasses1DataContext db = new DataClasses1DataContext();
        public int? getUserIdFromSession()
        {
            int? intid = 0;
            if (HttpContext.Current.Session["intUserId"] == null || HttpContext.Current.Session["intUserId"] == "")
            {
                return intid;
            }
            intid = Convert.ToInt32(HttpContext.Current.Session["intUserId"]);
            return intid;
        }
    }

}