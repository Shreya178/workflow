﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WFD.Models
{
    public class clsLogin
    {
        public int? intId { get; set; }
        public int? Type { get; set; }
        public string strName { get; set; }
        public string strEmail { get; set; }

        public string strPassword { get; set; }
        public string strResponse { get; set; }
        public clsLogin Login(clsLogin cls)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
           
         
                var data = (from item in db.tblLoginMsts where item.strEmail == cls.strEmail && item.strPassword == cls.strPassword && item.intUserType == clsConstant.UsertypeDesigner select item).FirstOrDefault();
            var data1 = (from item in db.tblLoginMsts where item.strEmail == cls.strEmail && item.strPassword == cls.strPassword && item.intUserType == clsConstant.UsertypeBeachTshirt select item).FirstOrDefault();

            if (data != null)
                {
                    cls.strEmail = data.strEmail;
                    cls.strPassword = data.strPassword;
                    cls.intId = data.intId;
                    cls.Type = data.intUserType;
                    cls.strResponse = clsConstant.MESSAGE_SUCCESS;
                }
            else if (data1 != null)
            {
                cls.strEmail = data1.strEmail;
                cls.strPassword = data1.strPassword;
                cls.intId = data1.intId;
                cls.Type = data1.intUserType;
                cls.strResponse = clsConstant.MESSAGE_SUCCESS1;
            }


            return cls;
        }

    }
}