﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WFD.Models
{
    public class clsDesigner
    {
        clsCommon objCommon = new clsCommon();
        public int? intId { get; set; }
        public int? intCategory { get; set; }

        public int? intQty { get; set; }
        public HttpPostedFileBase strProfile { get; set; }
        public int intSize { get; set; }
        public int? intStatus { get; set; }

        public string strColour { get; set; }
        public string Design { get; set; }
        public int strPId { get; set; }
        public string strMaterial { get; set; }
        public int intPrice { get; set; }
        public string strProductDetails { get; set; }
        public string strFileName { get; set; }
        public int? Type { get; set; }
        public string strResponse { get; set; }
        public string strDesign { get; set; }
        public string strSearchText { get; set; }
        public List<clsDesigner> LSTdesignList { get; set; }

    
        public List<clsDesigner> LStPurchaseList { get; set; }
        public int? intPageSize { get; set; }
        public int? intPageIndex { get; set; }
        public int? intTotalRecord { get; set; }
        public int? intPageCount { get; set; }
        public Pager Pager { get; set; }

        public int intTotalEntries { get; set; }
        public int intShowingEntries { get; set; }
        public int intfromEntries { get; set; }
        public string strCreatedDate { get; set; }
        public string strDueDate { get; set; }

        public clsDesigner InsertDesign(clsDesigner cls)
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
                DataClasses1DataContext db = new DataClasses1DataContext();
                conn.Open();
                SqlCommand cmd = new SqlCommand("Sp_Designer_Insert", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intId", SqlDbType.Int).Value = cls.intId;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = cls.Type;
                cmd.Parameters.Add("@strProfilePic", SqlDbType.VarChar).Value = cls.strFileName;
                cmd.Parameters.Add("@intGender", SqlDbType.Int).Value = cls.intCategory;
                cmd.Parameters.Add("@strDesign", SqlDbType.VarChar).Value = cls.strDesign;
                cmd.Parameters.Add("@intQty", SqlDbType.Int).Value = cls.intQty;
                cmd.Parameters.Add("@intSize", SqlDbType.Int).Value = cls.intSize;
                cmd.Parameters.Add("@strPId", SqlDbType.Int).Value = cls.strPId;
                cmd.Parameters.Add("@strMaterial", SqlDbType.VarChar).Value = cls.strMaterial;
                cmd.Parameters.Add("@intPrice", SqlDbType.VarChar).Value = cls.intPrice;
                cmd.Parameters.Add("@strProductDetail", SqlDbType.VarChar).Value = cls.strProductDetails;
                cmd.Parameters.Add("@intLoginId", SqlDbType.Int).Value = objCommon.getUserIdFromSession();
                cmd.Parameters.Add("@strColour", SqlDbType.VarChar).Value = cls.strColour;
                cmd.Parameters.Add("@EntityId", SqlDbType.Int).Value = SqlDbType.Int;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 0;
                da.ReturnProviderSpecificTypes = true;
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    string intRefId = dt.Rows[0][0].ToString();
                    if (intRefId == clsConstant.RECORD_EXISTS)
                    {
                        cls.strResponse = clsConstant.MESSAGE_EXISTS;
                    }
                    else
                    {
                        if (cls.intId == 0)
                        {
                            cls.strResponse = clsConstant.MESSAGE_SUCCESS;
                            cls.intId = Convert.ToInt32(intRefId);
                        }
                        else
                        {
                            cls.strResponse = clsConstant.MESSAGE_UPDATE;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw;
            }

            return cls;
        }
      


    }
}