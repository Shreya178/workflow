﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFD.Areas.BeachTShirt.Models;

namespace WFD.Areas.BeachTShirt.Controllers
{
    public class PurchaseOrderController : Controller   
    {
        // GET: BeachTShirt/PurchaseOrder
        DataClasses1DataContext db = new DataClasses1DataContext();
        clsCommon objCommon = new clsCommon();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult InsertPurchaseDetails(clsDesigner cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.InsertData;
                    cls = cls.InsertDetails(cls);
                    return Json(new { data = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }

            catch (Exception ex)
            {
                return RedirectToAction("Login", "Login");
            }
        }
        public ActionResult GetSingleItem(clsDesigner cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    tblDesignMst tbl = new tblDesignMst();
                    var getdata = (from r in db.tblDesignMsts where r.intId == cls.intId select r).FirstOrDefault();
                    return Json(data: getdata);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public ActionResult PurchaseOrder(clsDesigner cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);

            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    con.Open();
                    List<clsDesigner> lstDesignlist = new List<clsDesigner>();
                    SqlCommand cmd = new SqlCommand("Sp_Get_Design", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@intId", cls.intId);
                    cmd.Parameters.AddWithValue("@intPageSize", cls.intPageSize);
                    cmd.Parameters.AddWithValue("@intPageIndex", cls.intPageIndex);
                    cmd.Parameters.AddWithValue("@strSearchText", cls.strSearchText);
                    cmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    da.Fill(dt);
                    con.Close();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            clsDesigner obj = new clsDesigner();
                            obj.intId = Convert.ToInt32(dt.Rows[i]["intId"] == null || dt.Rows[i]["intId"].ToString().Trim() == "" ? null : dt.Rows[i]["intId"].ToString());
                            obj.intStatus = Convert.ToInt32(dt.Rows[i]["intStatus"] == null || dt.Rows[i]["intStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["intStatus"].ToString());
                            obj.strMaterial = dt.Rows[i]["strMaterial"] == null || dt.Rows[i]["strMaterial"].ToString().Trim() == "" ? null : dt.Rows[i]["strMaterial"].ToString();
                            obj.strPId = Convert.ToInt32(dt.Rows[i]["strPId"] == null || dt.Rows[i]["strPId"].ToString().Trim() == "" ? null : dt.Rows[i]["strPId"].ToString());
                            obj.intQty = Convert.ToInt32(dt.Rows[i]["intQty"] == null || dt.Rows[i]["intQty"].ToString().Trim() == "" ? null : dt.Rows[i]["intQty"].ToString());
                            obj.strDesign = dt.Rows[i]["strDesign"] == null || dt.Rows[i]["strDesign"].ToString().Trim() == "" ? null : dt.Rows[i]["strDesign"].ToString();
                            obj.Design = dt.Rows[i]["DesignFile"] == null || dt.Rows[i]["DesignFile"].ToString().Trim() == "" ? null : dt.Rows[i]["DesignFile"].ToString();
                            obj.intSize = Convert.ToInt32(dt.Rows[i]["intSize"] == null || dt.Rows[i]["intSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intSize"].ToString());
                            obj.intPrice = Convert.ToInt32(dt.Rows[i]["intPrice"] == null || dt.Rows[i]["intPrice"].ToString().Trim() == "" ? null : dt.Rows[i]["intPrice"].ToString());
                            //obj.strFileName = dt.Rows[i]["strDesign"] == null || dt.Rows[i]["strDesign"].ToString().Trim() == "" ? null : dt.Rows[i]["strDesign"].ToString();
                            obj.intPageCount = Convert.ToInt32(dt.Rows[i]["intPageCount"] == null || dt.Rows[i]["intPageCount"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageCount"].ToString());
                            obj.intCategory = Convert.ToInt32(dt.Rows[i]["intGender"] == null || dt.Rows[i]["intGender"].ToString().Trim() == "" ? null : dt.Rows[i]["intGender"].ToString());
                            obj.intPageSize = Convert.ToInt32(dt.Rows[i]["intPageSize"] == null || dt.Rows[i]["intPageSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageSize"].ToString());
                            obj.intPageIndex = Convert.ToInt32(dt.Rows[i]["intPageIndex"] == null || dt.Rows[i]["intPageIndex"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageIndex"].ToString());
                            obj.intTotalRecord = Convert.ToInt32(dt.Rows[i]["intTotalRecord"] == null || dt.Rows[i]["intTotalRecord"].ToString().Trim() == "" ? null : dt.Rows[i]["intTotalRecord"].ToString());
                            lstDesignlist.Add(obj);
                        }
                    }
                    cls.LSTdesignList = lstDesignlist;

                    return View("PurchaseOrder", cls);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                //objclsCommonFunction.SendErrorMail(ex, "");
                return RedirectToAction("Login", "Login");
            }
        }
    }
}