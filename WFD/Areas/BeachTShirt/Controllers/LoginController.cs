﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFD.Areas.BeachTShirt.Models;

namespace WFD.Areas.BeachTShirt.Controllers
{
    public class LoginController : Controller
    {
        // GET: BeachTShirt/Login
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(clsLogin cls)
        {
            try
            {
                cls.intId = 0;
                cls = cls.Login(cls);
                if (cls.intId > 0)
                {
                    Session["intUserId"] = cls.intId;
                    // Session["UserName"] = cls.strName;
                    Session["strEmail"] = cls.strEmail;
                    Session["strPassword"] = cls.strPassword;
                    Session["UserId"] = cls.intId;
                    Session["Type"] = cls.Type;

                    return Json(cls, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    cls.strResponse = clsConstant.MESSAGE_FAIL;
                }
                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in Login" + ex.Message);
            }
        }
    }
}