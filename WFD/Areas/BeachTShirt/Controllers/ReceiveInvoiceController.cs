﻿using WFD.Areas.BeachTShirt.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WFD.Areas.BeachTShirt.Controllers
{
    public class ReceiveInvoiceController : Controller
    {
        DataClasses1DataContext db = new DataClasses1DataContext();
        clsCommon objCommon = new clsCommon();
        // GET: BeachTShirt/ReceiveInvoice
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ShipTo()
        {
            return View();
        }
        public ActionResult ShipInvoice(clsInvoice cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    con.Open();

                    List<clsInvoice> lstinvoicelist = new List<clsInvoice>();
                    SqlCommand cmd = new SqlCommand("Sp_Get_ShipmentInvoiceDetail", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@intId", cls.intId);
                    cmd.Parameters.AddWithValue("@intPageSize", cls.intPageSize);
                    cmd.Parameters.AddWithValue("@intPageIndex", cls.intPageIndex);
                    cmd.Parameters.AddWithValue("@strSearchText", cls.strSearchText);
                    cmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    da.Fill(dt);
                    con.Close();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            clsInvoice obj = new clsInvoice();
                            obj.intId = Convert.ToInt32(dt.Rows[i]["intId"] == null || dt.Rows[i]["intId"].ToString().Trim() == "" ? null : dt.Rows[i]["intId"].ToString());
                            obj.strPId = Convert.ToInt32(dt.Rows[i]["ProductId"] == null || dt.Rows[i]["ProductId"].ToString().Trim() == "" ? null : dt.Rows[i]["ProductId"].ToString());
                            obj.deliveraddress = dt.Rows[i]["deliveraddress"] == null || dt.Rows[i]["deliveraddress"].ToString().Trim() == "" ? null : dt.Rows[i]["deliveraddress"].ToString();
                            //obj.intInvoiceNo = Convert.ToInt32(dt.Rows[i]["invoiceNumber"] == null || dt.Rows[i]["invoiceNumber"].ToString().Trim() == "" ? null : dt.Rows[i]["invoiceNumber"].ToString());
                            obj.pickaddress = dt.Rows[i]["pickaddress"] == null || dt.Rows[i]["pickaddress"].ToString().Trim() == "" ? null : dt.Rows[i]["pickaddress"].ToString();
                            obj.strDesign = dt.Rows[i]["design"] == null || dt.Rows[i]["design"].ToString().Trim() == "" ? null : dt.Rows[i]["design"].ToString();
                            obj.intPrice = Convert.ToInt32(dt.Rows[i]["Price"] == null || dt.Rows[i]["Price"].ToString().Trim() == "" ? null : dt.Rows[i]["Price"].ToString());
                            obj.intPageCount = Convert.ToInt32(dt.Rows[i]["intPageCount"] == null || dt.Rows[i]["intPageCount"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageCount"].ToString());
                            obj.intPageSize = Convert.ToInt32(dt.Rows[i]["intPageSize"] == null || dt.Rows[i]["intPageSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageSize"].ToString());
                            obj.intPageIndex = Convert.ToInt32(dt.Rows[i]["intPageIndex"] == null || dt.Rows[i]["intPageIndex"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageIndex"].ToString());
                            //obj.Status = Convert.ToInt32(dt.Rows[i]["intStatus"] == null || dt.Rows[i]["intStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["intStatus"].ToString());
                            obj.intTotalRecord = Convert.ToInt32(dt.Rows[i]["intTotalRecord"] == null || dt.Rows[i]["intTotalRecord"].ToString().Trim() == "" ? null : dt.Rows[i]["intTotalRecord"].ToString());
                            obj.intStatus = Convert.ToInt32(dt.Rows[i]["intStatus"] == null || dt.Rows[i]["intStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["intStatus"].ToString());
                            obj.DueDate = dt.Rows[i]["DueDate"] == null || dt.Rows[i]["DueDate"].ToString().Trim() == "" ? null : Convert.ToDateTime(dt.Rows[i]["DueDate"]).ToString("dd/MM/yyyy");

                            lstinvoicelist.Add(obj);

                        }
                    }
                    cls.LSTShipinvoicelist = lstinvoicelist;

                    return View("ShipInvoice", cls);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {

                return RedirectToAction("Login", "Login");
            }
        }
        public ActionResult Invoice(clsInvoice cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    con.Open();

                    List<clsInvoice> lstinvoicelist = new List<clsInvoice>();
                    SqlCommand cmd = new SqlCommand("Sp_Get_InvoiceDetail", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@intId", cls.intId);
                    cmd.Parameters.AddWithValue("@intPageSize", cls.intPageSize);
                    cmd.Parameters.AddWithValue("@intPageIndex", cls.intPageIndex);
                    cmd.Parameters.AddWithValue("@strSearchText", cls.strSearchText);
                    cmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    da.Fill(dt);
                    con.Close();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            clsInvoice obj = new clsInvoice();
                            obj.intId = Convert.ToInt32(dt.Rows[i]["intId"] == null || dt.Rows[i]["intId"].ToString().Trim() == "" ? null : dt.Rows[i]["intId"].ToString());
                            obj.strPId = Convert.ToInt32(dt.Rows[i]["ProductId"] == null || dt.Rows[i]["ProductId"].ToString().Trim() == "" ? null : dt.Rows[i]["ProductId"].ToString());
                            obj.intInvoiceNo =Convert.ToInt32(dt.Rows[i]["invoiceNumber"] == null || dt.Rows[i]["invoiceNumber"].ToString().Trim() == "" ? null : dt.Rows[i]["invoiceNumber"].ToString());
                            obj.strDesign = dt.Rows[i]["design"] == null || dt.Rows[i]["design"].ToString().Trim() == "" ? null : dt.Rows[i]["design"].ToString();
                            obj.intPrice = Convert.ToInt32(dt.Rows[i]["Price"] == null || dt.Rows[i]["Price"].ToString().Trim() == "" ? null : dt.Rows[i]["Price"].ToString());
                            obj.intPageCount = Convert.ToInt32(dt.Rows[i]["intPageCount"] == null || dt.Rows[i]["intPageCount"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageCount"].ToString());
                            obj.intPageSize = Convert.ToInt32(dt.Rows[i]["intPageSize"] == null || dt.Rows[i]["intPageSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageSize"].ToString());
                            obj.intPageIndex = Convert.ToInt32(dt.Rows[i]["intPageIndex"] == null || dt.Rows[i]["intPageIndex"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageIndex"].ToString());
                            //obj.Status = Convert.ToInt32(dt.Rows[i]["intStatus"] == null || dt.Rows[i]["intStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["intStatus"].ToString());
                            obj.intTotalRecord = Convert.ToInt32(dt.Rows[i]["intTotalRecord"] == null || dt.Rows[i]["intTotalRecord"].ToString().Trim() == "" ? null : dt.Rows[i]["intTotalRecord"].ToString());
                            obj.intStatus = Convert.ToInt32(dt.Rows[i]["intStatus"] == null || dt.Rows[i]["intStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["intStatus"].ToString());
                            obj.DueDate = dt.Rows[i]["DueDate"] == null || dt.Rows[i]["DueDate"].ToString().Trim() == "" ? null : Convert.ToDateTime(dt.Rows[i]["DueDate"]).ToString("dd/MM/yyyy");

                            lstinvoicelist.Add(obj);

                        }
                    }
                    cls.LSTinvoicelist = lstinvoicelist;

                    return View("Invoice", cls);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {

                return RedirectToAction("Login", "Login");
            }

        }
        public ActionResult UpdateStatus(clsInvoice cls)

        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    
                    tblInvoiceMst tbl = new tblInvoiceMst();
                    var updatestatus = (from r in db.tblInvoiceMsts where r.intId == cls.intId select r).FirstOrDefault();
                    updatestatus.intStatus = (updatestatus.intStatus == 1 ? 2 : 2);

                    db.SubmitChanges();
                    return Json(new { data = "status update" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public ActionResult UpdateShipStatus(clsInvoice cls)

        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {

                    tblShipMentInvoice tbl = new tblShipMentInvoice();
                    var updatestatus = (from r in db.tblShipMentInvoices where r.intId == cls.intId select r).FirstOrDefault();
                    updatestatus.intStatus = (updatestatus.intStatus == 1 ? 2 : 2);

                    db.SubmitChanges();
                    return Json(new { data = "status update" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public ActionResult InsertShipmentDtl(clsInvoice cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.InsertData;
                    cls = cls.InsertDetails(cls);
                    return Json(new { data = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }

            catch (Exception ex)
            {
                return RedirectToAction("Login", "Login");
            }
        }


    }
    }