﻿using System.Web.Mvc;

namespace WFD.Areas.BeachTShirt
{
    public class BeachTShirtAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BeachTShirt";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BeachTShirt_default",
                "BeachTShirt/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}