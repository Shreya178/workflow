﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WFD.Areas.BeachTShirt.Models
{
    public class clsConstant
    {
        public static int UsertypeDesigner = 1;
        public static int UsertypeBeachTshirt = 2;
        public static int InsertData = 1;
        public static int UpdateData = 1;
        public static int DeleteData = 2;
        public static string MESSAGE_SUCCESS = "success";
        public static string MESSAGE_OK = "ok";
        public static string MESSAGE_ERROR = "error";
        public static string MESSAGE_FAIL = "fail";
        public static string MESSAGE_DEPENDENCY = "dependency";
        public static string MESSAGE_EXISTS = "exists";
        public static string MESSAGE_UPDATE = "update";
        public static string RECORD_EXISTS = "409";
        public static string MESSAGE_NODATA = "norecord";
        public static string MESSAGE_LOGOUT = "logout";
    }
}