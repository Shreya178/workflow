﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WFD.Areas.BeachTShirt.Models
{
    public class clsLogin
    {
        public int? intId { get; set; }
        public int? Type { get; set; }
        public string strName { get; set; }
        public string strEmail { get; set; }

        public string strPassword { get; set; }
        public string strResponse { get; set; }
        public clsLogin Login(clsLogin cls)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            var data = (from item in db.tblLoginMsts where item.strEmail == cls.strEmail && item.strPassword == cls.strPassword && item.intUserType == clsConstant.UsertypeBeachTshirt select item).FirstOrDefault();

            if (data != null)
            {
                cls.strEmail = data.strEmail;
                cls.strPassword = data.strPassword;
                cls.intId = data.intId;
                cls.Type = data.intUserType;
                cls.strResponse = clsConstant.MESSAGE_SUCCESS;
            }
            return cls;
        }


    }
}