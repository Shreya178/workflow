﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WFD.Areas.BlueSkyLogistic.Controllers
{
    public class HomeController : Controller
    {
        // GET: BlueSkyLogistic/Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult dashboard()
        {
            return View();
        }
    }
}