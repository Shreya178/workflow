﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFD.Areas.BlueSkyLogistic.Data;

namespace WFD.Areas.BlueSkyLogistic.Controllers
{
    public class ShipmentController : Controller
    {
        // GET: BlueSkyLogistic/Shipment
        DataClasses1DataContext db = new DataClasses1DataContext();
        clsCommon objCommon = new clsCommon();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult InsertShipmentDtl(clsShipment cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.InsertData;
                    cls = cls.InsertDetails(cls);
                    return Json(new { data = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }

            catch (Exception ex)
            {
                return RedirectToAction("Login", "Login");
            }
        }

        public ActionResult ShipInvoice()
        {
            return View();
        }
        public ActionResult ShipmentDetails(clsShipment cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WFDdbConnectionString"].ConnectionString);
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    con.Open();

                    List<clsShipment> lstshipment = new List<clsShipment>();
                    SqlCommand cmd = new SqlCommand("Sp_Get_ShipmentDetail", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@intId", cls.intId);
                    cmd.Parameters.AddWithValue("@intPageSize", cls.intPageSize);
                    cmd.Parameters.AddWithValue("@intPageIndex", cls.intPageIndex);
                    cmd.Parameters.AddWithValue("@strSearchText", cls.strSearchText);
                    cmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    da.Fill(dt);
                    con.Close();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            clsShipment obj = new clsShipment();
                            obj.intId = Convert.ToInt32(dt.Rows[i]["intId"] == null || dt.Rows[i]["intId"].ToString().Trim() == "" ? null : dt.Rows[i]["intId"].ToString());
                            obj.strPId = Convert.ToInt32(dt.Rows[i]["ProductId"] == null || dt.Rows[i]["ProductId"].ToString().Trim() == "" ? null : dt.Rows[i]["ProductId"].ToString());
                          obj.strDesign = dt.Rows[i]["design"] == null || dt.Rows[i]["design"].ToString().Trim() == "" ? null : dt.Rows[i]["design"].ToString();
                            obj.intPrice = Convert.ToInt32(dt.Rows[i]["Price"] == null || dt.Rows[i]["Price"].ToString().Trim() == "" ? null : dt.Rows[i]["Price"].ToString());
                            obj.intPageCount = Convert.ToInt32(dt.Rows[i]["intPageCount"] == null || dt.Rows[i]["intPageCount"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageCount"].ToString());
                            obj.intPageSize = Convert.ToInt32(dt.Rows[i]["intPageSize"] == null || dt.Rows[i]["intPageSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageSize"].ToString());
                            obj.intPageIndex = Convert.ToInt32(dt.Rows[i]["intPageIndex"] == null || dt.Rows[i]["intPageIndex"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageIndex"].ToString());
                            //obj.Status = Convert.ToInt32(dt.Rows[i]["intStatus"] == null || dt.Rows[i]["intStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["intStatus"].ToString());
                            obj.intTotalRecord = Convert.ToInt32(dt.Rows[i]["intTotalRecord"] == null || dt.Rows[i]["intTotalRecord"].ToString().Trim() == "" ? null : dt.Rows[i]["intTotalRecord"].ToString());
                            obj.intStatus = Convert.ToInt32(dt.Rows[i]["intStatus"] == null || dt.Rows[i]["intStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["intStatus"].ToString());
                            obj.DueDate = dt.Rows[i]["DueDate"] == null || dt.Rows[i]["DueDate"].ToString().Trim() == "" ? null : Convert.ToDateTime(dt.Rows[i]["DueDate"]).ToString("dd/MM/yyyy");
                            obj.DeliverAddress = dt.Rows[i]["DeliverAddress"] == null || dt.Rows[i]["DeliverAddress"].ToString().Trim() == "" ? null : dt.Rows[i]["DeliverAddress"].ToString();
                            obj.PickAddress = dt.Rows[i]["PickAddress"] == null || dt.Rows[i]["PickAddress"].ToString().Trim() == "" ? null : dt.Rows[i]["PickAddress"].ToString();

                            lstshipment.Add(obj);

                        }
                    }
                    cls.LSTShipment = lstshipment;

                    return View("ShipmentDetails", cls);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {

                return RedirectToAction("Login", "Login");
            }
        }
    }
}