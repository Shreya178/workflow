﻿using System.Web.Mvc;

namespace WFD.Areas.BlueSkyLogistic
{
    public class BlueSkyLogisticAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BlueSkyLogistic";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BlueSkyLogistic_default",
                "BlueSkyLogistic/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}